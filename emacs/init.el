(load-theme 'modus-vivendi t)

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))
(setq create-lockfiles nil)


(set-face-attribute 'default nil
                    ;;  :family "Roboto Mono"
                    ;;  :weight 'light
                    :height 40)

(display-time-mode 1)

(setq org-agenda-files '("/home/vamshi/personal/agenda-file.org"))
;;     "/path/to/another/agenda-file.org"))


(defun personal-file ()
  (interactive)
  (find-file (expand-file-name "/home/vamshi/personal/agenda-file.org")))

;; ~/.config/home-manager/emacs/init.el")))

(defun private-config ()
  (interactive )
  (find-file (expand-file-name "~/.config/home-manager/emacs/init.el")))


(defun home-manager-dir ()
  (interactive )
  (find-file (expand-file-name "~/.config/home-manager/")))


(require 'dired)

(put 'dired-find-alternate-file 'disabled nil)
;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
(add-hook 'dired-mode-hook 'dired-hide-details-mode)


(toggle-frame-tab-bar)
(setq compile-command "nix run")
(defun my-compile()
  "Execute compile directly"
  (interactive)
  (execute-kbd-macro (kbd "M-x compile RET")))
(global-set-key (kbd "C-c m") 'my-compile)

(defun insert-printf-c()
  "Insert printf c"
  (interactive)
  (insert "printf();")
  (forward-char -2)
  )

;; (global-set-key (kbd "C-c p") 'insert-printf-c)
(defun custom-c-mode-hook()
  (local-set-key (kbd "C-c p") 'insert-printf-c))

(add-hook 'c-mode-hook 'custom-c-mode-hook)

(global-set-key (kbd "C-c n") 'forward-list)
(global-set-key (kbd "C-c b") 'backward-list)
